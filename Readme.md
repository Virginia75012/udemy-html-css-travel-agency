# Cours UDEMY John Codeur

### Apprendre HTML CSS et créer un site Web

Apprentissage du HTML et CSS par la pratique.
Tout d'abord révision des bases et par la suite mise en place d'une première page factice d'une Agence de Voyage "Travel Agency".

Revoyons les bases HTML :
- syntaxe
- structure d'une page
- paragraphes et titres
- tags spéciaux
- liens
- images
- listes
- attibut style
- divs
- formulaires

Revoyons les bases CSS :
- différentes façons d'écrire du CSS
- syntaxe
- couleurs
- unités de mesures
- polices
- sélecteurs
- propriété display
- modèle de boîte
- propriété Float
- propriété position

Déploiement sur Heroku :
https://travel-agency-udemy.herokuapp.com/